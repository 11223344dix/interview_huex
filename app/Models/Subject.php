<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;
    public function module(){
        return $this->hasMany(Module::class);
    }

    public function teacher(){
        return $this->hasOne(Teacher::class);
    }

    public function students(){
        return $this->belongsToMany(Student::class);
    }
}
