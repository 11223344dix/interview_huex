<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    
    public function user(){
        return $this->belongsTo(User::class);
        
    }

    public function clas(){
        return $this->belongsTo(Clas::class);
        
    }

    public function subjects(){
        return $this->belongsToMany(Subject::class);
    }

}
