<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class authAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $type = session('type');

        if ($type !== "admin") {
            return redirect('adminaceessError');
        }


        return $next($request);
    }
}
