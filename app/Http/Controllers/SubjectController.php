<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SubjectController extends Controller
{

    public function store(Request $request)
    {
        $subject = new Subject;
        $subject->subject_name = $request->input('subject_name');
        $subject->save();

        return "subject successfully added";
    }

    public function show(){
        $subject = DB::table('modules')
        ->select('modules')
        ->get();

        return $subject;


    }
}
