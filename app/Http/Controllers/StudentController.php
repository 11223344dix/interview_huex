<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Clas;
use App\Models\User;
use App\Models\Subject;
use App\Models\StudentSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function signIn(Request $req){
        $user=new User;
        
        $user->email=$req->email;
        $user->type='student';
        $user->password=$req->password;
        $user->save();

        $class=clas::where('class_name','=',$req->class_name)->first();

        $user1=User::where('email','=',$req->email)->value('id');
        $student=new Student;
        $student->user_id=$user1;
        $student->first_name=$req->first_name;
        $student->last_name=$req->last_name;
        $student->class_id=$class['id'];
        $student->save();

        return  $student->first_name." profile created";

    }


    public function updateProfile(Request $req){


        $currentuserid=session('loginid');
        $class=clas::where('class_name','=',$req->class_name)->first();
        $student=Student::where('user_id','=',$currentuserid)->first();

        $student->first_name=$req->first_name;
        $student->last_name=$req->last_name;
        $student->class_id=$class['id'];

        $student->save();
        
        return  $student->first_name." profile updated";

    }


    public function addstudentsub(Request $req){
        $currentuserid=session('loginid');
    
        $currentuser=Student::where('user_id','=',$currentuserid)->first();
        $currentuser1=Student::find($currentuser['id']);

        $subid=$req->input('subjectid');

        $currentuser1->subjects()->sync($subid);


     
      return "subjects added successfully";

    }


    public function subscribe(Request $req){
        $currentuserid=session('loginid');
        $currentuser=Student::where('user_id','=',$currentuserid)->first();
        $currentuser1=Student::find($currentuser['id']);
      $id2= $currentuser1['id'];

        $subject_name=$req->input('subject');

        $subject=Subject::where('subject_name','=',$subject_name)->first();
     
        $subject_id= $subject['id'];
 
        // $studentsubject=StudentSubject::where('subject_id','=', 1)->first();
    
        $studentsubject = DB::table('student_subject')->where('subject_id','=', $subject_id)->first();

        $id=$studentsubject->subject_id;
     
        $uid=$studentsubject->student_id;
        $subscribed=$studentsubject->subscribed;
   
        if($id==$subject_id &&  $uid==$id2){
            if($subscribed==false){
                DB::table('student_subject')
                ->where('subject_id', $id ) 
                ->where('student_id', $uid) 
                ->update(['subscribed' => true]);
                return "successfully subcribed";
            }
           else{
                DB::table('student_subject')
                ->where('subject_id', $id ) 
                ->where('student_id', $uid) 
                ->update(['subscribed' => false]);
                return "successfully Un-subcribed";
            }
           
        }else{
            return "you cant acess other subjects ";
        }

        

    }


    public function showsubscribeSubject(Request $request){
        $currentuserid=session('loginid');
        $currentuser=Student::where('user_id','=',$currentuserid)->first();
        $currentuser1=Student::find($currentuser['id']);
      $id2= $currentuser1['id'];

      
        $subSubject=Student::find( $id2)->Subject;

        return  $subSubject;
    }
}

