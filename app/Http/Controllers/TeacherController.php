<?php

namespace App\Http\Controllers;
use App\Models\Clas;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Subject;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function signIn(Request $req){
        $user=new User;
        
        $user->email=$req->email;
        $user->type='teacher';
        $user->password=$req->password;
        $user->save();
        $subjectName= $req->subject_name;
        $sub_id=Subject::where('subject_name','=',$subjectName)->value('id');

        $class=clas::where('class_name','=',$req->class_name)->first();

        $user1=User::where('email','=',$req->email)->value('id');
        $teacher=new Teacher;
        $teacher->user_id=$user1;
        $teacher->first_name=$req->first_name;
        $teacher->last_name=$req->last_name;
        $teacher->dob=$req->dob;
        $teacher->class_id=$class['id'];
        $teacher->subject_id=$sub_id;
        $teacher->qualification=$req->qualification;
 
        $teacher->save();
       
        return $user;
    }

    public function updateProfile(Request $req){
        $currentuserid=session('loginid');
        $class=clas::where('class_name','=',$req->class_name)->first();
        $teacher=Teacher::where('user_id','=',$currentuserid)->first();

        $teacher->first_name=$req->first_name;
        $teacher->last_name=$req->last_name;
        $teacher->dob=$req->dob;
        $teacher->qualification=$req->qualification;
        $teacher->class_id=$class['id'];
        $teacher->save();
        

    }


    public function show(){
        $currentuserid=session('loginid');
        $currentuser=Teacher::where('user_id','=',$currentuserid)->first();
        $currentuser1=Teacher::find($currentuser['id']);
      $id2= $currentuser1['id'];


        $subject=Teacher::find($id2)->Subject;

        return $subject;
    }
}
