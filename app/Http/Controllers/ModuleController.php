<?php

namespace App\Http\Controllers;
use App\Models\Module;
use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    public function add(Request $req){

        $currentuserid=session('loginid');
  
        $teacher=Teacher::where('user_id','=',$currentuserid)->first();
   

        $subject=Subject::where('id','=',$teacher->subject_id)->first();
   
        $module=new Module;
        $module->modules=$req->modules;
        $module->description=$req->description;
        $module->subject_id=$subject['id'];
        $module->save();

        return $module;

      
       

    }

    public function update(Request $request, $id){
        $currentuserid=session('loginid');
  
        $teacher=Teacher::where('user_id','=',$currentuserid)->first();


        $modulesname=$request->input('modules');
        $description=$request->input('description');
        $module=Module::find($id);
        if($module->subject_id== $teacher->subject_id){
        
            $module->modules=$modulesname;
            $module->description=$description;
            $module->save();
        }
        else{
            
            return "you cant modify others subject";
        }
       

        return $module;
        
      
       

    }
}
