<?php

namespace App\Http\Controllers;
use App\Models\Clas;
use Illuminate\Http\Request;

class ClasController extends Controller
{
    public function add(Request $req){

        $class=new Clas;
        $class->class_name=$req->input('class_name');
        $class->save();

        return "class added successfully!!";
    }
}
