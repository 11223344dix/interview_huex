<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('home', function () {
    return view('welcome');
});


Route::get('studentaceessError', function () {
    return "you are not a student. Access denied!!!!!!";
});

Route::get('teacheraceessError', function () {
    return "you are not a teacher. Access denied!!!!!!"; 
});


Route::get('adminaceessError', function () {
    return "you are not an Admin. Access denied!!!!!!"; ;
});