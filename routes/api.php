<?php

use Illuminate\Http\Request;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ClasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>['teacheracess']],function(){
   
    Route::post("addmodule",[ModuleController::class,'add']);
    Route::put("updatemodule/{id}",[ModuleController::class,'update']);
    Route::put("updateTeacherProfile",[TeacherController::class,'updateProfile']);
    Route::get("teacherSubject",[TeacherController::class,'show']);
 
    
});



Route::group(['middleware'=>['studentacess']],function(){
     Route::put("subscribe",[StudentController::class,'subscribe']);
    Route::put("updateStudentProfile",[StudentController::class,'updateProfile']);
    Route::post("addstudentsubject",[StudentController::class,'addstudentsub']);
    Route::get("showsubscribeSubject",[StudentController::class,'showsubscribeSubject']);
});






Route::group(['middleware'=>['adminacess']],function(){
  
    Route::post("addclass",[ClasController::class,'add']);
    Route::post("addsubject",[SubjectController::class,'store']); 
   
});




Route::get('showsubject',[SubjectController::class,'show']);

Route::post("studentregister",[StudentController::class,'signIn']); 
Route::post("techerregister",[TeacherController::class,'signIn']);
Route::post("login",[UserController::class,'login']);
